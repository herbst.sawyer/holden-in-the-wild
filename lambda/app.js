const express = require('express')
const fs = require('fs');
const app = express()
const cors = require('cors');
const AWS = require('aws-sdk');
const s3 = new AWS.S3();

app.use(cors());

app.get('/images', (req, res) => {
    var params = {
        Bucket: "holden-photos", 
        MaxKeys: 1000
    };

    s3.listObjectsV2(params, (error, data) => {
        if(error) {
            res.status(500).json({error: 'Database error'});
            console.log(error)
        } else {
            var items = data.Contents.map(item => item.Key.split('/'));
            var directories = items.map(item => {
                if(!item[1]) {
                    return item[0];
                } else {
                    return;
                }
            })
            directories = directories.filter(d => d);
            var images = items.filter(item => {
                if(item[1]) {
                    return item;
                } else {
                    return;
                }
            });
            images = images.filter(i => i);
            var data = images.map(image => {
                return {
                    source: {
                        uri: `http://holden-photos.s3-website.us-east-2.amazonaws.com/${image.join('/')}`
                    },
                    caption: `@${image[0]}`
                }
            })
            res.status(200).json({ data });
        }
    })
})

module.exports = app;