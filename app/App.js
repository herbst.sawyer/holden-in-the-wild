import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from './components/HomeScreen';
import RandomImageScreen from './components/RandomImageScreen';

const MainNavigator = createStackNavigator({
  Home: {screen: HomeScreen},
  RandomImage: {screen: RandomImageScreen}
})

const App = createAppContainer(MainNavigator);

export default App;