import Axios from 'axios';

export default {
    random: function() {
        return new Promise((resolve, reject) => {
            Axios.get('https://0syf0zl3t8.execute-api.us-east-2.amazonaws.com/dev/images/').then(res => {
                var images = res.data.data;
                            
                const image = images[
                    Math.floor(Math.random() * images.length)
                ];

                resolve(image);
            }).catch(err => {
                reject(err);
            })
        })
    }
}