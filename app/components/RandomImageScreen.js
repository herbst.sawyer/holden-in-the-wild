import React, { Component } from 'react';
import {
    View,
    StyleSheet
} from 'react-native';

import {
    Text,
    Image,
    Button
} from 'react-native-elements';

import Images from '../Images';

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'powderblue'
    },

    container: {
        alignItems: 'center',
        justifyContent: 'center'
    },

    caption: {
        marginTop: 20,
        fontStyle: 'italic',
        fontSize: 20
    },

    moreButton: {
        width: '100%',
        marginTop: 50
    }
})

class RandomImageScreen extends Component {
    constructor(props) {
        super(props);
        this.state = { image: null }
        this.getNewImage = this.getNewImage.bind(this);
    }

    componentDidMount() {
        this.getNewImage()
    }

    getNewImage() {
        Images.random().then(image => {
            this.setState({ image });
        }).catch(error => console.log(error));
    }

    static navigationOptions = {
        title: 'Random Holden Image'
    }

    render() {
        const image = this.state.image;
        if(image) {
            var { height, width, source, caption } = image;
            height = height ? height : 500;
            width = width ? width : 300;

            const buttonTextItems = [
                'Give Me More!',
                'I Can\'t Stop!',
                'Hand Over the Next One!'
            ]

            const buttonText = buttonTextItems[Math.floor(Math.random() * buttonTextItems.length)];

            return (
                <View style={styles.screen}>
                    <View style={styles.container}>
                        <Image
                            source={source}
                            style={{ height, width }}
                        />
                        <Text style={styles.caption}>
                            {caption}
                        </Text>
                        <Button
                            buttonStyle={styles.moreButton}
                            title={`${buttonText} 🌶`}
                            onPress={this.getNewImage}
                        />
                    </View>
                </View>
            );
        } else {
            return <View></View>
        }
    }
}
 
export default RandomImageScreen;