import React, { Component } from 'react';
import {
    StyleSheet,
    View
} from 'react-native';
import {
    Button,
    Text
} from 'react-native-elements';

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'powderblue',
    },

    button: {
        width: '100%',
        marginTop: 20
    },

    header: {
        margin: 10
    }
});

class HomeScreen extends Component {
    constructor(props) {
        super(props);
    }

    static navigationOptions = {
        title: 'Home'
    }

    render() {
        const { navigate } = this.props.navigation;
        return (
            <>
            <View style={styles.screen}>
                <Text h2 style={styles.header}>
                    Holden in the Wild
                </Text>
                <Text>
                    Get the finest quality pictures of holden.
                </Text>
                <Button
                    buttonStyle={styles.button}
                    title="Random Photo 🔥"
                    onPress={() => navigate('RandomImage')}
                />
            </View>
            </>
        );
    }
}
 
export default HomeScreen;